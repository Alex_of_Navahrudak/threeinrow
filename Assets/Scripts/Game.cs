﻿using System;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public enum FigureType
{
    Empty,
    BlueCircle,
    GreenRhombus,
    RedSquare,
    YellowTriangle
}

public class Game : MonoBehaviour
{
    [SerializeField] private Sprite[] figures;
    private Button[,] cells;
    private Model gameModel;
    private float fallingSpeed = 5;

    void Start()
    {
        gameModel = new Model(SetFigure);
        InitCells();
        gameModel.Start();
    }

    public void SetFigure(int x, int y, FigureType type)
    {
        cells[x, y].GetComponent<Image>().sprite = figures[(int)type];
    }

    public void Click()
    {
        string name = EventSystem.current.currentSelectedGameObject.name;
        int cellNumber = GetNumber(name);
        int x = cellNumber % Model.SIZE;
        int y = cellNumber / Model.SIZE;

        gameModel.Click(x,y);
    }

    private void InitCells()
    {
        cells = new Button[Model.SIZE, Model.SIZE];

        for (int i = 0; i < cells.Length; i++)
        {
            GameObject buttonObj = GameObject.Find($"Button ({i})");
            Button button = buttonObj.GetComponent<Button>();
            cells[i % Model.SIZE, i / Model.SIZE] = button;
        }
    }

    private int GetNumber(string name)
    {
        Regex regex = new Regex("\\((\\d+)\\)");
        Match match = regex.Match(name);
        if (!match.Success)
            throw new System.Exception("Please check the cell name...");
        Group group = match.Groups[1];
        string number = group.Value;
        return Convert.ToInt32(number);
    }
}
