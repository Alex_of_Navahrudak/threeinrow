﻿using System;
using System.Collections.Generic;

public delegate void SetFigure(int x, int y, FigureType type);

public class Model
{
    public const int SIZE = 5;
    public const int FIGURESCOUNT = 5;

    private SetFigure setFigure;

    private (int x, int y) fromCell;
    private bool isCellClicked;

    private FigureType[,] grid;
    private bool[,] marks = new bool[SIZE, SIZE];

    private int fallingFiguresCount = 0;
    public int FallingFiguresCount
    {
        get => fallingFiguresCount;
        set => fallingFiguresCount = (value >= 0) ? value : 0;
    }

    System.Random random = new System.Random();

    public Model(SetFigure setFigure)
    {
        this.setFigure = setFigure;
        grid = new FigureType[SIZE,SIZE];
    }

    public void Start()
    {
        ClearGrid();
        SetRandomFigures();
        isCellClicked = false;
    }

    public void Click(int x, int y)
    {
        if (!isCellClicked && grid[x, y] > 0)
            TakeFigure(x, y);
        else if (isCellClicked)
            PutFigure(x, y);
    }

    private void TakeFigure(int x, int y)
    {
        fromCell = (x, y);
        isCellClicked = true;
    }

    private void ExchangeFigures((int x, int y) sourceFigure, (int x, int y) targetFigure)
    {
        FigureType sourceCellFigure = grid[sourceFigure.x, sourceFigure.y];
        FigureType targetCellFigure = grid[targetFigure.x, targetFigure.y];

        SetFigureIntoCell(targetFigure.x, targetFigure.y, sourceCellFigure);
        SetFigureIntoCell(sourceFigure.x, sourceFigure.y, targetCellFigure);
    }

    private void PutFigure(int x, int y)
    {
        if (!isCellClicked) return;
        isCellClicked = false;

        if (!CanMove(x,y)) return;
        if (Math.Abs(fromCell.x - x) > 1 || Math.Abs(fromCell.y - y) > 1) return;

        ExchangeFigures((x, y), fromCell);
        
        if (!CalculateLines())
        {
            ExchangeFigures((x, y), fromCell);
        }
    }

    private void ClearCell(int x, int y)
    {
        grid[x, y] = FigureType.Empty;
        setFigure(x, y, FigureType.Empty);
        marks[x, y] = false;
    }

    private void ClearGrid()
    {
        for (int i = 0; i < SIZE; i++)
            for (int j = 0; j < SIZE; j++)
                ClearCell(i,j);
    }

    private void SetFigureUsingGravity(int x, int y)
    {
        for (int line = y; line >= 0; line--)
        {
            if (grid[x, y] == (int)FigureType.Empty && grid[x, line] != (int)FigureType.Empty)
            {
                FallingFiguresCount++;
                ExchangeFigures((x, line), (x, y));
                return;
            }
        }

        SetRandomFigures();
    }

    private void SetFiguresUsingGravity()
    {
        for (int y = SIZE - 1; y > 0; y--)
            for (int x = 0; x < SIZE; x++)
                if (grid[x, y] == 0)
                    SetFigureUsingGravity(x, y);

        SetRandomFigures();
    }

    public void SetRandomFigures()
    {
        for (int i = 0; i < SIZE; i++)
            for (int j = 0; j < SIZE; j++)
                if (grid[i,j] == 0)
                    SetRandomFigure(i, j);

        if (CalculateLines())
            SetRandomFigures();
    }

    public void SetRandomFigure(int x, int y)
    {
        int randomFigure = 1 + random.Next(FIGURESCOUNT - 1);
        grid[x, y] = (FigureType)randomFigure;
        setFigure(x, y, (FigureType)randomFigure);
    }

    private void SetFigureIntoCell(int x, int y, FigureType figure)
    {
        grid[x, y] = figure;
        setFigure(x, y, figure);
        marks[x, y] = false;
    }

    private int CalculateLine(int x, int y, int directionX, int directionY)
    {
        FigureType currentFigure = grid[x, y];

        if (currentFigure == FigureType.Empty) return 0;

        int count = 0;
        List<(int x, int y)> indexes = new List<(int, int)>();

        for (int xPos = x, yPos = y; GetCell(xPos, yPos) == currentFigure; xPos += directionX, yPos += directionY)
        {
            indexes.Add((xPos, yPos));
            count++;
        }

        if (count >= 3)
        {
            foreach (var index in indexes)
            {
                marks[index.x, index.y] = true;
            }

            return count;
        }

        return 0;
    }

    public bool CalculateLines()
    {
        int repetitiveCells = 0;

        for (int i = 0; i < SIZE; i++)
        {
            for (int j = 0; j < SIZE; j++)
            {
                repetitiveCells += CalculateLine(i, j, 1, 0);
                repetitiveCells += CalculateLine(i, j, 0, 1);
            }
        }

        if (repetitiveCells > 0)
        {
            for (int i = 0; i < SIZE; i++)
            {
                for (int j = 0; j < SIZE; j++)
                {
                    if (marks[i, j])
                        ClearCell(i, j);
                }
            }

            SetFiguresUsingGravity();
            return true;
        }

        return false;
    }

    private FigureType GetCell(int x, int y)
    {
        return CheckIfCellOnGrid(x, y) ? grid[x, y] : 0;
    }

    private bool CheckIfCellOnGrid(int x, int y)
    {
        return (x >= 0 && x < SIZE) &&
               (y >= 0 && y < SIZE);
    }

    private bool CanMove(int x, int y)
    {
        return true;
    }
}
